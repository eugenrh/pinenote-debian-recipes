# How I have used the debian-installer to install Debian trixie (testing) on an encrypted partition on the PineNote

This documents the above endeavor. The procedure consists of many manual steps and requires good knowledge of our current PineNote boot process.

My main goal was to have an encrypted Debian installation on my PineNote, alongside the Android one and the current `bookworm` that I used until now.

I could have learn how to use `cryptsetup` and `lvm` to prepare an encrypted volume and copy my existing `bookworm` there... but I think that any Linux capable device should have the option to install Debian using the amazing `debian-installer` (d-i). And `debian-installer` knows very well how to use `lvm` and `cryptsetup` and many more things. So I've chosen to learn how to use it on my PineNote.

Disclaimer: I knew nothing about `debian-installer` internals and how it works. My knowledge here is based on that couple of days I managed to figure it out and use it on the PineNote.

From my few considerations and tries on using the installer on the PineNote, I finally managed to do it by what I think now to be the simpler and easier option so far. I'll describe that in the following.

## The Concept

The `debian-installer` (d-i) is usually booted from a CD or a USB stick. I don't know how to do that on PineNote. And the USB port is needed for plugging in a keyboard to be used during installation. However, the d-i, at the minimum, is just an init ramdisk (initrd). And we already know how to boot and use those. So the idea is to use our PineNote kernel and firmware, d-i's `initrd` and our way of booting them using the u-boot's `extlinux.conf` mechanism. `/boot` will be on a separate unencrypted bootable partition. ***And*** at the end of installation, **don't reboot** right away, but enter a shell and install ***our*** kernel (and removing Debian's) and everything to make sure you get a bootable new system. You must know our booting process... I'm not providing every detail here..

## Getting and preparing the debian-installer (aka its initrd)

Since I was doing a fresh installation, I chose to install Debian Trixie (testing). And for that, Debian recommends us to use the daily built images of the installer. We want only the init ramdisk so it's enough to use the `netboot` d-i. So just grab `initrd.gz` from this download directory: https://d-i.debian.org/daily-images/arm64/daily/netboot/debian-installer/arm64/

The **first problem** we have is that the initrd contains the Debian kernel's components (like modules) and know nothing about our kernel or firmware. So let's change it.

Inird is a `cpio` arhive. So after gunzip it, extract the files into a directory `Initrd`. I was 'root' when doing this with:
```
# cpio --file initrd -imd --no-absolute-filenames -D Initrd
```
Update that `Initrd` directory with our kernel modules and the Pinenote's firmware (`/usr/lib/firmware` directory from an existing Debian OS on PN).
And here is the commands to repack the archive:
```
# cd Initrd
# find . | cpio -o -H newc > ../myinitrd
```

## Booting preparations, the extlinux.conf file

What remains is to boot our new `myinitrd` init ramdisk. You could add a new entry into your existing `/boot/extlinux/extlinux.conf` file. But I've already created a small `ext2` partition for booting the installer from it. So I used that partition and have only one entry into `extlinux.conf`:
```
label l0
        menu label Debian Installer netboot testing
        linux /boot/vmlinuz-6.6.11-gfb3beac8ca58
        initrd /boot/mynetbootinitrd
        fdt /boot/rk3566-pinenote-v1.2.dtb
```
It contains the kernel file, `vmlinuz...` the installer initrd and the device tree. No need for any kernel parameters.

## Running the installer

After you're succesfully booting it, the installer will display on the e-ink panel its dialogues. It is able to use the display because we've provided the `rockchip_ebc` driver module inside the initrd, and also the waveform through that firmware directory. Use a keyboard to interact with the installer. The wifi is detected without problems. After connecting it to a network, the installer would download its components and other Debian packages. At one point it will complain that it couldn't find our kernel version in the Debian archive, so it can't download its modules. But we already provided them and they are used as needed. It's not a problem.

At the `partman` dialogues (Partition Manager) I used 'Manual' and  proceed in creating a `lvm` group and volumes, and an encrypted volume for `/`. I've already had an `ext2` partition made for `/boot` (kept as a simple partition, so no lvm or encryption on this one).

At task selection (`tasksel`) I've chosen `standard Debian` and `openssh server`, but no GUI manager. I will install `sway` later.

When it finished and asked for a reboot, I've said **"No"** (or was it **"Go back"**, I don't remember). Then the installer provided a menu with actions and I chose to enter a shell so I could made the system bootable!

### Make the system bootable before finishing the installation
The shell gives access to your new system which is mounted as `/target`. So you need to chroot into it, like this:
```
cd /target
mount --rbind /sys sys/
chroot /target /bin/bash
```
 So I've started by installing some useful tools like `vim` or `aptitude` to make configuration easier.

Then I've installed the **`u-boot-menu`** package. That provides `u-boot-update` script and hooks that makes sure the `extlinux.conf` file is recreated after every kernel change. The content of that file is configured by `/etc/default/u-boot` file -- copy it from your old Debian installation (mount its partition to /mnt) for example. **Important:** since we have separate partitions and the device tree file (.dtb) is on the encrypted one, you must add `U_BOOT_SYNC_DTBS="true"` to the `u-boot` file. With that, the `dtb`s are copied onto the /boot partition whenever a kernel is added or reconfigured. See: https://salsa.debian.org/debian/u-boot-menu/-/commit/51d120d549e5b21a19ce659c7bef578c86ed9636

OK, now install our kernel deb package and remove the Debian one ( remove the `linux-image-arm64` metapackage that will also ends up in removing the specific Debian kernel version). 

I've managed to boot successfully from the first time, but I still had some problems that can be fixed at this stage before rebooting from the installer. Here are the fixes I did:

a) Add these lines to `/etc/initramfs-tools/modules` file:
```
rockchip_ebc
wusb3801
```
and recreate the init ramdisk with `# update-initramfs -u`. In this way those kernel modules (including their firmware, like the waveform file) for the e-ink and USB-C port controller are available and used at `initrd` "time" when it asks for the LUKS passphrase at the console. So you can see it and can have a functional usb keyboard to enter it.

b) The problem is that the passphrase is asked **only** inside the last console specified on the Linux kernel command line.
In our case we have these consoles specified: `console=tty0 console=ttyS2,1500000n8` , so by default it only asks inside the serial console. You can switch the order and have it ask only on the PineNote. But I prefer to have both options so I've hacked the `u-boot-update` script to generate three menu labels for each kernel: with password prompt on PineNote console, with it on the serial console, and the old recovery option.

c) the last problem I've had is that by default the gpu was disabled if not used after 30 seconds. If I would take too long to type the passphrase... no more gpu, no more anything, really. The fix was to change the device tree and set up the gpu's regulator to be always on.

